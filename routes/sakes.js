'use strict';

var express = require('express');
var router = express.Router();

router.get('/', function(req, res) {
  var db = req.db;
  var collection = db.get('sakes');
  collection.find({}, function(e, docs) {
    res.json(docs);
  });
});

router.get('/:sakeId', function(req, res) {
  var db = req.db;
  var sakeId = req.params.sakeId;
  var collection = db.get('sakes');
  collection.findById(sakeId, function(e, docs) {
    res.json(docs);
  });
});

module.exports = router;
