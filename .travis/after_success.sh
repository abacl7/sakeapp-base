#!/bin/bash

if [ "$TRAVIS_PULL_REQUEST" = "false" ]; then
  curl -i -H "Content-Type: application/json" --data '{"source_type": "Branch", "source_name": "'"$TRAVIS_BRANCH"'"}' -X POST $DOCKERHUB_TRIGGER_URL
fi