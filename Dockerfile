FROM node:0.12

MAINTAINER tktk8924 <myphone.tk@gmail.com>

WORKDIR /app
COPY ./ .

RUN npm install
RUN node_modules/.bin/bower --allow-root install

EXPOSE 3000
CMD ["sh", "bin/run.sh"]