'use strict';

var sakeControllers = angular.module('sakeControllers', []);

sakeControllers.controller('SakeListCtrl', ['$scope', '$http',
    function($scope, $http) {
        $http.get('/api/sakes').then(
            function success(response) {
                $scope.sakes = response['data'];
                $scope.http_status = response['status'];
            }
        );
    }
]);

sakeControllers.controller('SakeDetailCtrl', ['$scope', '$routeParams', '$http',
    function($scope, $routeParams, $http) {
        $http.get('/api/sakes/' + $routeParams.sakeId).then(
            function success(response) {
                $scope.sake = response['data'];
                $scope.http_status = response['status'];
            }
        );
    }
]);
