'use strict';

var sakeApp = angular.module('sakeApp', [
  'ngRoute',
  'sakeControllers'
]);

sakeApp.config(['$routeProvider',
  function($routeProvider) {
    $routeProvider.
      when('/sakes', {
        templateUrl: 'sakes/list.html',
        controller: 'SakeListCtrl'
      }).
      when('/sakes/:sakeId', {
        templateUrl: 'sakes/detail.html',
        controller: 'SakeDetailCtrl'
      }).
      otherwise({
        redirectTo: '/sakes'
      });
  }
]);