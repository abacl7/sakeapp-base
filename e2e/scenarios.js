'use strict';

describe('SakeApp', function() {
  var searchInput;

  it('should automatically redirect to /sakes when location hash/fragment is empty', function() {
    browser.get('');
    expect(browser.getLocationAbsUrl()).toMatch("/sakes");
  });

  beforeEach(function() {
    browser.get('');
    searchInput = element(by.model('query'));
  });

  describe('List', function() {

    it('should list 4 sake objects when filtered by "t"', function() {
      var repeatSake = 'sake in sakes | filter:{brand:query}';
      var sakeObjects = element.all(by.repeater(repeatSake));
      expect(sakeObjects.count()).toEqual(16);

      searchInput.sendKeys('t');
      sakeObjects = element.all(by.repeater(repeatSake));
      expect(sakeObjects.count()).toEqual(4);
    });

  });

  describe('Detail', function() {

    it('should render a detail page', function () {
      searchInput.sendKeys('tatenokawa');
      var repeatSake = 'sake in sakes | filter:{brand:query}';
      var sakeObjects = element.all(by.repeater(repeatSake));
      expect(sakeObjects.count()).toEqual(1);

      sakeObjects.first().element(by.css('.btn')).click();
      expect(element(by.binding('sake.brand')).getText()).toBe('Tatenokawa');
    });

  });

});
