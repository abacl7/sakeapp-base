/**
 * Module dependencies
 */
var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var morgan = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var http = require('http');
var mongo = require('mongodb');
var monk = require('monk');
var routes = require('./routes');
var sakes = require('./routes/sakes');

var app = module.exports = express();


/**
 * Configuration
 */
app.set('port', process.env.PORT || 3000);

app.use(morgan('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

var mongoHost = process.env.MONGODB_HOST || 'localhost';
var mongoPort = process.env.MONGODB_PORT || '27017';
var mongoDB = process.env.MONGODB_DB || 'sakeapp';

// add db connection object to http requests
app.use(function(req, res, next) {
  req.db = monk(mongoHost + ':' + mongoPort + '/' + mongoDB);
  next();
});


/**
 * Routes
 */
app.use('/', routes);
app.use('/api/sakes', sakes);


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

var env = process.env.NODE_ENV || 'development';

// development error handler
if (env === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.send(err);
  });
}

// production error handler
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.send(err.message);
});


/**
 * Start Server
 */
http.createServer(app).listen(app.get('port'), function() {
  console.log('Express server listening on port ' + app.get('port'));
});
