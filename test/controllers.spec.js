'use strict';

describe('sakeControllers', function() {
  var scope, ctrl, httpBackend;
  var sakeId = '565d629a2e28cf45d9597dc3';
  var fakeRes = {_id: sakeId, brand: 'Horaisen', type: 'Tokubetsu-Junmai'};

  beforeEach(module('sakeApp'));
  beforeEach(inject(function ($rootScope, $controller, $httpBackend) {
    scope = $rootScope.$new();
    httpBackend = $httpBackend;
  }));

  describe('SakeListCtrl', function () {

    beforeEach(inject(function ($controller) {
      ctrl = $controller('SakeListCtrl', {$scope: scope});
    }));

    it('should create "sakes" model', function () {
      httpBackend.expectGET('/api/sakes').respond(fakeRes);

      httpBackend.flush();
      expect(scope.sakes).toEqual(fakeRes);
      expect(scope.http_status).toEqual(200);
    });

  });

  describe('SakeDetailCtrl', function() {

    beforeEach(inject(function($controller, $routeParams) {
      $routeParams.sakeId = sakeId;
      ctrl = $controller('SakeDetailCtrl', {$scope: scope});
    }));

    it('should create "sake" model', function() {
      httpBackend.expectGET('/api/sakes/' + sakeId).respond(fakeRes);

      httpBackend.flush();
      expect(scope.sake).toEqual(fakeRes);
      expect(scope.http_status).toEqual(200);
    });

  });

});
